# LoggingExample

Example project about logging.

## simple logging

    dotnet add package Serilog
    dotnet add package Serilog.Sinks.Console
    dotnet add package Serilog.Sinks.File

## Check out Program.cs

    using Serilog;
    ...
    Log.Information(...)

## Check out Controllers Api1Controller and Api2Controller

Logging directly or via attribute are both possible.

## Logger attribute

Utilities/LoggerAttribute.cs

## Register / configure

    // Program.cs

    Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .WriteTo.File("logs/log.txt",
        rollingInterval: RollingInterval.Day,
        rollOnFileSizeLimit: true)
    .CreateLogger();

## Not used yet

    dotnet add package Serilog.AspNetCore
