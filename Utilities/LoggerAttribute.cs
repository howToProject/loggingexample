using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace LoggingExample.Utilities;

public class LoggerAttribute : ActionFilterAttribute
{
    public override void OnActionExecuting(ActionExecutingContext actionContext)
    {
        var controllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)actionContext.ActionDescriptor).ControllerName;
        var actionName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)actionContext.ActionDescriptor).ActionName;
        Log.Information("Api called: {ControllerName}, {MethodName}!",
            controllerName,
            actionName);
    }
}
