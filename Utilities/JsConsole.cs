using Microsoft.JSInterop;

namespace LoggingExample.Utilities;

public class JsConsole
{
    private readonly IJSRuntime _jsRuntime;
    public JsConsole(IJSRuntime jSRuntime)
    {
        _jsRuntime = jSRuntime;
    }

    public async Task LogAsync(object message)
    {
        await _jsRuntime.InvokeVoidAsync("console.log", message);
    }
}