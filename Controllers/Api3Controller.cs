using Microsoft.AspNetCore.Mvc;

namespace LoggingExample.Controllers;

[ApiController]
[Route("[controller]")]
public class Api3Controller : Controller
{
    // GET
    public string Index()
    {
        return "api3";
    }
}