using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace LoggingExample.Controllers;

[ApiController]
[Route("[controller]")]
public class Api1Controller : Controller
{
    // GET
    public string Index()
    {
        Log.Information("Api called: {ControllerName}, {MethodName}!",
            ControllerContext.ActionDescriptor.ControllerName,
            ControllerContext.ActionDescriptor.ActionName);
        return "api1";
    }
}