using LoggingExample.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace LoggingExample.Controllers;

[ApiController]
[Route("[controller]")]
public class Api2Controller : Controller
{
    // GET
    [Logger]
    public string Index()
    {
        return "api2";
    }
}
